<?php

namespace common\social_network;

use Yii;
use common\models\PostAccount;

class SocialNetworkFactory
{
    /**
     * @param string $name
     * @param PostAccount $postAccount
     * @return SocialNetworkInterface
     */
    public static function get($name, PostAccount $postAccount)
    {
        return new $name($postAccount);
    }
}