<?php

namespace common\social_network;

use yii\base\Model;
use common\models\Proxy;
use common\models\PostAccount;

/**
 * @property Proxy $proxy
 * @property bool $isGroup
 * @property bool $isPage
 * @property bool $isUser
 * @property string $id
 * @property string $cookie
 * @property string $accessToken
 * @property string $accountType
 * @property string $token
 * @property string $tokenSecret
 * @property string $consumerKey
 * @property string $consumerSecret
 * @property string $applicationKey
 * @property string $clientSecret
 * @property array $oath1Config
 * @property string $login
 * @property string $password
 */
abstract class SocialNetworkAbstract extends Model
{
    /**
     * @var string
     */
    private $_login;

    /**
     * @var string
     */
    private $_password;

    /**
     * @var Proxy
     */
    private $_proxy;

    /**
     * @var string
     */
    private $_id;

    /**
     * @var string
     */
    private $_cookie;

    /**
     * @var string
     */
    private $_access_token;

    /**
     * @var string
     */
    private $_account_type;

    /**
     * @var string
     */
    private $_token;

    /**
     * @var string
     */
    private $_token_secret;

    /**
     * @var string
     */
    private $_consumer_key;

    /**
     * @var string
     */
    private $_consumer_secret;

    /**
     * @var string
     */
    private $_application_key;

    /**
     * @var string
     */
    private $_client_secret;


    /**
     * @return Proxy
     */
    public function getProxy()
    {
        return $this->_proxy;
    }

    /**
     * @param Proxy $proxy
     */
    public function setProxy($proxy)
    {
        $this->_proxy = $proxy;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return string
     */
    public function getCookie()
    {
        return $this->_cookie;
    }

    /**
     * @param string $cookie
     */
    public function setCookie($cookie)
    {
        $this->_cookie = $cookie;
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        return $this->_access_token;
    }

    /**
     * @param string $access_token
     */
    public function setAccessToken($access_token)
    {
        $this->_access_token = $access_token;
    }

    /**
     * @return string
     */
    public function getAccountType()
    {
        return $this->_account_type;
    }

    /**
     * @param string $account_type
     */
    public function setAccountType($account_type)
    {
        $this->_account_type = $account_type;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->_token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->_token = $token;
    }

    /**
     * @return string
     */
    public function getTokenSecret()
    {
        return $this->_token_secret;
    }

    /**
     * @param string $token_secret
     */
    public function setTokenSecret($token_secret)
    {
        $this->_token_secret = $token_secret;
    }

    /**
     * @return string
     */
    public function getConsumerKey()
    {
        return $this->_consumer_key;
    }

    /**
     * @param string $consumer_key
     */
    public function setConsumerKey($consumer_key)
    {
        $this->_consumer_key = $consumer_key;
    }

    /**
     * @return string
     */
    public function getConsumerSecret()
    {
        return $this->_consumer_secret;
    }

    /**
     * @param string $consumer_secret
     */
    public function setConsumerSecret($consumer_secret)
    {
        $this->_consumer_secret = $consumer_secret;
    }

    /**
     * @return string
     */
    public function getApplicationKey()
    {
        return $this->_application_key;
    }

    /**
     * @param string $application_key
     */
    public function setApplicationKey($application_key)
    {
        $this->_application_key = $application_key;
    }

    /**
     * @return string
     */
    public function getClientSecret()
    {
        return $this->_client_secret;
    }

    /**
     * @param string $client_secret
     */
    public function setClientSecret($client_secret)
    {
        $this->_client_secret = $client_secret;
    }

    /**
     * @return bool
     */
    public function getIsGroup()
    {
        return $this->accountType == PostAccount::SOCIAL_NETWORK_ACCOUNT_TYPE_GROUP;
    }

    /**
     * @return bool
     */
    public function getIsPage()
    {
        return $this->accountType == PostAccount::SOCIAL_NETWORK_ACCOUNT_TYPE_PAGE;
    }

    /**
     * @return bool
     */
    public function getIsUser()
    {
        return $this->accountType == PostAccount::SOCIAL_NETWORK_ACCOUNT_TYPE_USER;
    }

    /**
     * @param FIle[] $files
     * @return array
     */
    public function saveAttachments($files) {
        $attachments = [];
        if ($files) {
            foreach ($files as $file) {
                $attachments[] = $this->saveAttachment($file);
            }
        }
        return $attachments;
    }

    /**
     * @param $file
     * @return string|null
     */
    public function saveAttachment($file)
    {
        if ($file->isVideo) {
            return $this->saveVideoAttachment($file);
        } else {
            return $this->savePhotoAttachment($file);
        }
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->_login;
    }

    /**
     * @param string $login
     */
    public function setLogin($login)
    {
        $this->_login = $login;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->_password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->_password = $password;
    }
}
