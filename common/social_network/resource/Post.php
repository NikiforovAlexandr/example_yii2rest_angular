<?php

namespace common\social_network\resource;

use yii\base\Model;

/**
 * @property string $id
 * @property string $code
 * @property string $datetimeCreate
 * @property integer $like
 * @property integer $comment
 * @property integer $repost
 * @property Comment[] $comments
 * @property string $lastCommentId
 */
class Post extends Model
{
    /**
     * @var string
     */
    private $_id;

    /**
     * @var string
     */
    private $_code;

    /**
     * @var string
     */
    private $_datetime_create;

    /**
     * @var integer
     */
    private $_like;

    /**
     * @var integer
     */
    private $_comment;

    /**
     * @var integer
     */
    private $_repost;

    /**
     * @var Comment[]
     */
    private $_comments;

    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->_code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->_code = $code;
    }

    /**
     * @return string
     */
    public function getDatetimeCreate()
    {
        return $this->_datetime_create;
    }

    /**
     * @param string $datetime_create
     */
    public function setDatetimeCreate($datetime_create)
    {
        $this->_datetime_create = $datetime_create;
    }

    /**
     * @return int
     */
    public function getLike()
    {
        return $this->_like;
    }

    /**
     * @param int $like
     */
    public function setLike($like)
    {
        $this->_like = $like;
    }

    /**
     * @return int
     */
    public function getComment()
    {
        return $this->_comment;
    }

    /**
     * @param int $comment
     */
    public function setComment($comment)
    {
        $this->_comment = $comment;
    }

    /**
     * @return int
     */
    public function getRepost()
    {
        return $this->_repost;
    }

    /**
     * @param int $repost
     */
    public function setRepost($repost)
    {
        $this->_repost = $repost;
    }

    /**
     * @return Comment[]
     */
    public function getComments()
    {
        return $this->_comments;
    }

    /**
     * @param Comment[] $comments
     */
    public function setComments($comments)
    {
        $this->_comments = $comments;
    }
}
