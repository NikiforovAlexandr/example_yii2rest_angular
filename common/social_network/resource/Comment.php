<?php

namespace common\social_network\resource;

use yii\base\Model;

/**
 * @property string $id
 * @property string $datetimeCreate
 * @property string $message
 * @property User $user
 */
class Comment extends Model
{
    /**
     * @var string
     */
    private $_id;

    /**
     * @var string
     */
    private $_datetime_create;

    /**
     * @var string
     */
    private $_message;

    /**
     * @var User
     */
    private $_user;

    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return string
     */
    public function getDatetimeCreate()
    {
        return $this->_datetime_create;
    }

    /**
     * @param string $datetime_create
     */
    public function setDatetimeCreate($datetime_create)
    {
        $this->_datetime_create = $datetime_create;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->_message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->_message = $message;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->_user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->_user = $user;
    }
}
