<?php

namespace common\social_network\resource;

use yii\base\Model;

/**
 * @property string $id
 * @property string $name
 * @property string $picture
 */
class User extends Model
{
    /**
     * @var string
     */
    private $_id;

    /**
     * @var string
     */
    private $_name;

    /**
     * @var string
     */
    private $_picture;

    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return string
     */
    public function getPicture()
    {
        return $this->_picture;
    }

    /**
     * @param string $picture
     */
    public function setPicture($picture)
    {
        $this->_picture = $picture;
    }
}
