<?php

namespace common\social_network\resource;

use yii\base\Model;

/**
 * @property integer $id
 * @property integer $owner_id
 * @property string $url
 * @property string $job
 */
class UploadServer extends Model
{
    /**
     * @var integer
     */
    private $_id;

    /**
     * @var integer
     */
    private $_owner_id;

    /**
     * @var string
     */
    private $_url;

    /**
     * @var
     */
    private $_job;

    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return int
     */
    public function getOwner_id()
    {
        return $this->_owner_id;
    }

    /**
     * @param int $owner_id
     */
    public function setOwner_id($owner_id)
    {
        $this->_owner_id = $owner_id;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->_url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->_url = $url;
    }

    /**
     * @return mixed
     */
    public function getJob()
    {
        return $this->_job;
    }

    /**
     * @param mixed $job
     */
    public function setJob($job)
    {
        $this->_job = $job;
    }
}
