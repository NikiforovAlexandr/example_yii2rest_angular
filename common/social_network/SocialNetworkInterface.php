<?php

namespace common\social_network;

use common\models\File;
use common\social_network\resource\Post;
use common\social_network\resource\Comment;

interface SocialNetworkInterface
{
    /**
     * @return Post[]
     */
    public function getPosts();

    /**
     * @param string $content
     * @param File[] $files
     * @return Post
     */
    public function createPost($content, $files = null);

    /**
     * @param string $social_network_post_id
     * @return bool
     */
    public function deletePost($social_network_post_id);

    /**
     * @param string $social_network_post_id
     * @return Comment[]
     */
    public function getComments($social_network_post_id);

    /**
     * @param string $social_network_post_id
     * @param string $content
     * @return Comment
     */
    public function createComment($social_network_post_id, $content);

    /**
     * @param string $social_network_post_id
     * @param integer $social_network_comment_id
     * @return bool
     */
    public function deleteComment($social_network_post_id, $social_network_comment_id);

    /**
     * @param string $method
     * @param string $uri
     * @param array $params
     * @param array $options
     * @return object
     */
    public function request($method, $uri, $params = [], $options = []);
}
