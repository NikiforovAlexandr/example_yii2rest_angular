<?php

namespace common\social_network;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\PostAccount;
use common\models\File;
use common\social_network\resource\Comment;
use common\social_network\resource\Post;
use common\social_network\resource\User;
use common\social_network\resource\UploadServer;
use common\helper\ContentHelper;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;

/**
 * @property integer $ownerID
 */
class Vkontakte extends SocialNetworkAbstract implements SocialNetworkInterface
{
    public $base_uri = 'https://api.vk.com/method/';

    /**
     * @param PostAccount $postAccount
     */
    public function __construct(PostAccount $postAccount)
    {
        $this->setId($postAccount->social_network_user_id);
        $this->accessToken($postAccount->social_network_access_token);
        $this->accountType($postAccount->social_network_account_type);
    }

    /**
     * @return Post[]
     */
    public function getPosts()
    {
        $params = [
            'owner_id' => $this->id,
            'count' => 100,
        ];
        $response = $this->request('post', 'wall.get', $params);

        if (!$this->hasErrors()) {
            if (isset($response->response->items) && count($response->response->items)) {
                $posts = [];

                foreach ($response->response->items as $item) {
                    $posts[] = new Post([
                        'id' => $item->id,
                        'like' => $item->likes->count,
                        'comment' => $item->comments->count,
                        'repost' => $item->reposts->count,
                    ]);
                }
                return $posts;
            }
        }
    }

    /**
     * @param string $content
     * @param File[] $files
     * @return Post
     */
    public function createPost($content, $files = null)
    {
        $attachments = $this->saveAttachments($files);

        if (!$this->hasErrors()) {
            $params = [
                'owner_id' => $this->id,
                'message' => $content,
                'attachments' => $attachments ? implode(',', $attachments) : null,
            ];
            if (!$this->isUser) {
                $params['from_group'] = true;
            }
            $response = $this->request('post', 'wall.post', $params);

            if (!$this->hasErrors()) {
                return new Post([
                    'id' => $response->response->id
                ]);
            }
        }
    }

    /**
     * @param string $social_network_post_id
     * @return bool
     */
    public function deletePost($social_network_post_id)
    {
        $params = [
            'owner_id' => $this->id,
            'post_id' => $social_network_post_id,
        ];
        $response = $this->request('POST', 'wall.delete', $params);

        if (!$this->hasErrors()) {
            return ($response->response == 1);
        }
    }

    /**
     * @param integer $social_network_post_id
     * @return Comment[]
     */
    public function getComments($social_network_post_id)
    {
        $params = [
            'owner_id' => $this->id,
            'post_id' => $social_network_post_id,
            'offset' => 0,
            'count' => 100,
            'preview_length' => 0,
            'extended' => 1,
        ];

        $response = $this->request('post', 'wall.getComments', $params);

        if (!$this->hasErrors()) {
            return $this->prepareComments($response);
        }
    }

    /**
     * @param object $response
     * @return Comment[]
     */
    private function prepareComments($response)
    {
        $users = [];
        $comments = [];

        if ($response->response->count) {
            if ($response->response->profiles) {
                foreach ($response->response->profiles as $user) {
                    $users[$user->id] = new User([
                        'id' => $user->id,
                        'name' => $user->last_name . ' ' . $user->first_name,
                        'picture' => $user->photo_100
                    ]);
                }
            }

            if (isset($response->response->groups)) {
                foreach ($response->response->groups as $group) {
                    $users['-' . $group->id] = new User([
                        'id' => $group->id,
                        'name' => $group->name,
                        'picture' => $group->photo_100,
                    ]);
                }
            }

            foreach ($response->response->items as $items) {
                $comments[] = new Comment([
                    'id' => $items->id,
                    'datetimeCreate' => date('Y-m-d H:i:s', $items->date),
                    'message' => $items->text,
                    'user' => $users[$items->from_id]
                ]);
            }
        }

        return $comments;
    }


    /**
     * @param integer $social_network_post_id
     * @param string $content
     * @return Comment
     */
    public function createComment($social_network_post_id, $content)
    {
        $params = [
            'owner_id' => $this->id,
            'post_id' => $social_network_post_id,
            'message' => $content,
            'preview_length' => 0,
            'extended' => 1,
            'from_group' => 1,
        ];

        $response = $this->request('post', 'wall.createComment', $params);

        if (!$this->hasErrors()) {
            return new Comment([
                'id' => $response->response->id
            ]);
        }
    }

    /**
     * @param string $social_network_post_id
     * @param integer $social_network_comment_id
     * @return bool
     */
    public function deleteComment($social_network_post_id, $social_network_comment_id)
    {
        $params = [
            'owner_id' => $this->id,
            'comment_id' => $social_network_comment_id,
        ];
        $response = $this->request('post', 'wall.deleteComment', $params);

        if (!$this->hasErrors()) {
            return ($response->response == 1);
        }
    }

    /**
     * @param $file
     * @return string
     */
    protected function saveVideoAttachment($file)
    {
        $uploadServer = $this->getVideoUploadServer();
        if ($uploadServer) {

            $options = [
                'multipart' => [
                    [
                        'name' => 'file',
                        'contents' => fopen($file->serverFileName, 'r'),
                        'filename' => $file->fullName,
                    ],
                    [
                        'name' => 'wallpost',
                        'contents' => 0, //требуется ли после сохранения опубликовать запись с видео на стене (1 — требуется, 0 — не требуется).
                    ]
                ]
            ];

            if (!$this->isUser) {
                $options['multipart'][] =
                    [
                        'name' => 'group_id',
                        'contents' => $this->ownerID
                    ];
            }

            $response = $this->request('post', $uploadServer->url, '', $options);

            if (!$this->hasErrors()) {
                return 'video' . $uploadServer->owner_id . '_' . $response->video_id;
            }
        }
    }

    /**
     * @return UploadServer
     */
    private function getVideoUploadServer()
    {
        $params = [];
        if (!$this->isUser) {
            $params['group_id'] = $this->ownerID;
        }
        $response = $this->request('get', 'video.save', $params);

        if (!$this->hasErrors()) {
            return new UploadServer([
                'url' => $response->response->upload_url,
                'owner_id' => $response->response->owner_id,
            ]);
        }
    }

    /**
     * @param File $file
     * @return string
     */
    protected function savePhotoAttachment($file)
    {
        $uploadServer = $this->getPhotoUploadServer();
        if (!$this->hasErrors()) {
            $options = [
                'multipart' => [
                    [
                        'name' => 'photo',
                        'contents' => fopen($file->preview('vkontakte')->serverFileName, 'r'),
                        'filename' => $file->fullName,
                    ],
                    [
                        'name' => 'access_token',
                        'contents' => $this->accessToken,
                    ]
                ]
            ];

            $response = $this->request('post', $uploadServer->url, '', $options);

            if (!$this->hasErrors()) {
                $params = [
                    'photo' => $response->photo,
                    'server' => $response->server,
                    'hash' => $response->hash,
                ];

                if ($this->isUser) {
                    $params['user_id'] = $this->ownerID;
                } else {
                    $params['group_id'] = $this->ownerID;
                }
                $response = $this->request('post', 'photos.saveWallPhoto', $params);

                if (!$this->hasErrors()) {
                    return 'photo' . $response->response[0]->owner_id . '_' . $response->response[0]->id;
                }
            }
        }
    }

    /**
     * @return UploadServer
     */
    private function getPhotoUploadServer()
    {
        $params = [];

        if (!$this->isUser) {
            $params['group_id'] = $this->ownerID;
        }
        $response = $this->request('get', 'photos.getWallUploadServer', $params);

        if (!$this->hasErrors()) {
            return new UploadServer([
                'url' => $response->response->upload_url,
                'owner_id' => $response->response->user_id,
            ]);
        }
    }

    /**
     * @return integer
     */
    public function getOwnerID()
    {
        if (!$this->isUser) {
            return $this->id * -1;
        } else {
            return $this->id;
        }
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $params
     * @param array $options
     * @return object
     */
    public function request($method, $uri, $params = [], $options = [])
    {
        try {
            $client = new Client([
                'base_uri' => $this->base_uri,
            ]);

            if (!$options) {
                $key = ($method == 'get') ? 'query' : 'form_params';
                $options[$key] = $params;
                $options[$key]['access_token'] = $this->accessToken;
                $options[$key]['v'] = '5.53';
                $options['timeout'] = 3;
            }

            $request = $client->request($method, $uri, $options);
            $json = \GuzzleHttp\json_decode($request->getBody());

            if (isset($json->error)) {
                $this->addError('vkontakte', $json->error->error_msg);
            } else {
                return $json;
            }
        } catch (ClientException $e) {
            $this->addVkontakteError($e->getResponse()->getBody());
        } catch (RequestException $e) {
            $this->addError('vkontakte', 'Ошибка запроса');
        } catch (ServerException $e) {
            $this->addError('vkontakte', 'Ошибка сервера');
        } catch (ConnectException $e) {
            $this->addError('vkontakte', 'Ошибка соединения');
        }
    }

    /**
     * @param $string
     */
    protected function addVkontacteError($string)
    {
        $json = \GuzzleHttp\json_decode($string);
        $this->addError('vkontakte', $json->error->message);
    }
}
