<?php

namespace api\controllers\post;

use Yii;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;
use yii\web\Response;
use common\filters\auth\HttpBearerAuth;
use common\controllers\ControllerTrait;
use common\models\PostPublication;
use common\models\search\PostPublicationSearch;
use common\models\PostPublication2File;
use common\models\PostPublication2PostAccount;
use common\rbac\Rbac;

class PublicationController extends Controller
{
    use ControllerTrait;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'authenticator' => [
                'class' => HttpBearerAuth::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete'],
                'except' => ['options'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can(Rbac::POST_PUBLICATION_INDEX, ['postProject' => $this->postProject]);
                        }
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can(Rbac::POST_PUBLICATION_VIEW, ['postProject' => $this->postPublication->project, 'postPublication' => $this->postPublication]);
                        }
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can(Rbac::POST_PUBLICATION_CREATE, ['postProject' => $this->postProject]);
                        }
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can(Rbac::POST_PUBLICATION_UPDATE, ['postProject' => $this->postPublication->project, 'postPublication' => $this->postPublication]);
                        }
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can(Rbac::POST_PUBLICATION_DELETE, ['postProject' => $this->postPublication->project, 'postPublication' => $this->postPublication]);
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['GET', 'HEAD'],
                    'view' => ['GET', 'HEAD'],
                    'create' => ['POST'],
                    'update' => ['PUT', 'PATCH'],
                    'delete' => ['DELETE'],
                ],
            ],
        ]);
    }

    /**
     * @return PostPublication[]
     */
    public function actionIndex()
    {
        $postProject = $this->postProject;

        $date = Yii::$app->request->get('date');

        $searchModel = new PostPublicationSearch();
        return $searchModel->forProject($postProject->id)->forDate($date)->search(Yii::$app->request->queryParams);
    }

    /**
     * @return PostPublication
     */
    public function actionView()
    {
        return $this->postPublication;
    }

    /**
     * @return PostPublication
     */
    public function actionCreate()
    {
        $postProject = $this->postProject;

        $postPublication = new PostPublication();
        $postPublication->setPostProjectID($postProject->id);
        $postPublication->setProjectGmtOffset($postProject->timezone->gmt_offset);

        if ($postPublication->load(Yii::$app->request->post(), '') && $postPublication->save()) {
            $this->saveToPostPublication2PostAccount($postPublication->id, $postPublication->accountIDs);
            $this->saveToFile($postPublication->id, $postPublication->fileIDs);
        }

        return $postPublication;
    }

    /**
     * @return PostPublication
     */
    public function actionUpdate()
    {
        $postPublication = $this->postPublication;

        if ($postPublication->load(Yii::$app->request->post(), '') && $postPublication->save()) {
            $this->deleteFromPostPublication2PostAccount($postPublication->id);
            $this->saveToPostPublication2PostAccount($postPublication->id, $postPublication->accountIDs);

            $this->deleteFromFile($postPublication->id);
            $this->saveToFile($postPublication->id, $postPublication->fileIDs);
        }

        return $postPublication;
    }

    /**
     * @return PostPublication
     */
    public function actionDelete()
    {
        $postPublication = $this->postPublication;
        $postPublication->safeDelete();
        return $postPublication;
    }

    /**
     * @param integer $post_publication_id
     * @return bool
     */
    private function deleteFromPostPublication2PostAccount($post_publication_id)
    {
        PostPublication2PostAccount::updateAll([
            'status' => PostPublication2PostAccount::STATUS_DELETED
        ], [
            'post_publication_id' => $post_publication_id,
        ]);

        return true;
    }

    /**
     * @param integer $post_publication_id
     * @param array $accountIDs
     * @return bool
     */
    private function saveToPostPublication2PostAccount($post_publication_id, $accountIDs)
    {
        if (is_array($accountIDs)) {
            foreach ($accountIDs as $accountID) {
                PostPublication2PostAccount::loadOrCreatePostAccount($post_publication_id, $accountID);
            }
            return true;
        }
        return false;
    }

    /**
     * @param int $post_publication_id
     * @return bool
     */
    private function deleteFromFile($post_publication_id)
    {
        PostPublication2File::deleteAll([
            'post_publication_id' => $post_publication_id,
        ]);
        return true;
    }

    /**
     * @param int $post_publication_id
     * @param array $fileIDs
     * @return bool
     */
    private function saveToFile($post_publication_id, $fileIDs)
    {
        if ($fileIDs) {
            foreach ($fileIDs as $file_id) {
                $postPublication2File = new PostPublication2File([
                    'post_publication_id' => $post_publication_id,
                    'file_id' => $file_id
                ]);
                $postPublication2File->save();
            }
        }
        return true;
    }
}
