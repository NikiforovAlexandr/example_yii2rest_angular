'use strict';

const POST_STATUS_DELETED = 0;
const POST_STATUS_PUBLISHED = 1;
const POST_STATUS_PUBLISHING = 2;
const POST_STATUS_STOP_BECAUSE_HAS_ERROR = 3;
const POST_STATUS_WAITING = 5;

spaApp_publication.factory('PostPublication', ['RestModel', 'File',
    function (RestModel, File) {

        function PostPublication(data) {
            this.servicePath = 'post/publications';
            this.path = 'publication';
            if (data) {
                this.setData(data);
            }
        }

        PostPublication.prototype = Object.create(RestModel.prototype);

        PostPublication.prototype.constructor = PostPublication;

        PostPublication.prototype.files = [];

        PostPublication.prototype.accounts = [];

        PostPublication.prototype.afterSetData = function () {
            var self = this;
            self.prepareFiles();
        };

        PostPublication.prototype.prepareFiles = function () {
            var self = this;
            var files = [];
            angular.forEach(self.files, function (data) {
                var file = new File(data);
                this.push(file);
            }, files);
            self.files = files;
        };

        PostPublication.prototype.hasContent = function () {
            var self = this;
            return self.content ? true : false;
        };

        PostPublication.prototype.isPublished = function () {
            var self = this;
            return (self.post_status == POST_STATUS_PUBLISHED);
        };

        return PostPublication;

    }]);
