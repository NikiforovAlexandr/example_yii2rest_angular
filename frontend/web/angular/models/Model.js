'use strict';

spaApp_model.factory('Model', ['Flash',
    function (Flash) {

        function Model() {
        }

        Model.prototype = {
            _isLoading: false,
            _original: false,
            _errors: [],
            _params: {},

            addParams: function (params) {
                angular.extend(this._params, params);
                return this;
            },
            getParams: function () {
                var self = this;
                return self._params;
            },
            getParam: function (key) {
                var self = this;
                return self._params[key];
            },
            cleanParams: function () {
                var self = this;
                return self._params = [];
            },
            setData: function (data) {
                var self = this;
                angular.extend(self, data);
                self._afterSetData(data);
            },
            _afterSetData: function () {
                var self = this;
                self.afterSetData();
            },
            afterSetData: function () {
            },
            selIsLoading: function (isLoading) {
                var self = this;
                self._isLoading = isLoading;
            },
            getIsLoading: function () {
                var self = this;
                return self._isLoading;
            },
            getResponseError: function () {
                var self = this;
                return self._responseError;
            },
            _beforeSave: function () {
                var self = this;
                self.selIsLoading(true);
                self.beforeSave();
            },
            beforeSave: function () {
            },
            _afterSave: function (response) {
                var self = this;
                self.selIsLoading(false);
                self.cleanErrors();
                self.setData(response.data);
                self.afterSave(response);
            },
            afterSave: function (response) {
            },
            _beforeDelete: function () {
                var self = this;
                self.selIsLoading(true);
                self.beforeDelete();
            },
            beforeDelete: function () {
            },
            _afterDelete: function (response) {
                var self = this;
                self.selIsLoading(false);
                self.cleanErrors();
                self.setData(response.data);
                self.afterDelete();
            },
            afterDelete: function () {
            },
            _beforeGet: function () {
                var self = this;
                self.selIsLoading(true);
                self.beforeGet();
            },
            beforeGet: function () {
            },
            _afterGet: function (response) {
                var self = this;
                self.selIsLoading(false);
                self.setData(response.data);
                self.afterGet(response);
            },
            afterGet: function (response) {
            },
            cleanErrors: function () {
                var self = this;
                self._errors = [];
            },
            getErrors: function () {
                var self = this;
                return self._errors;
            },
            addErrors: function (errors) {
                var self = this;
                angular.forEach(errors, function (error) {
                    self.addError(error);
                });
            },
            addError: function (error) {
                var self = this;
                self.getErrors().push(error);
            },
            addResponseErrors: function (response) {
                var self = this;
                if (response.data.message !== undefined) {
                    self.addError(response.data.message);
                } else {
                    angular.forEach(response.data, function (error) {
                        self.addError(error.message);
                    });
                }
            },
            prepareResponseErrors: function (response) {
                var self = this;
                self.cleanErrors();
                self.addResponseErrors(response);
                self.selIsLoading(false);
                self.showFirstError();
            },
            showFirstError: function () {
                var self = this;
                var errors = self.getErrors();
                new Flash(errors[0]);
            },
            hasErrors: function () {
                var self = this;
                return self.getErrors().length > 0 ? true : false;
            },
            deleteFromData: function (index) {
                var self = this;
                self.data.splice(index, 1);
            },
            getByIndex: function (index) {
                var self = this;
                return self.data[index];
            }
        };

        return Model;

    }]);
