'use strict';
spaApp_model.factory('File', ['RestModel',
    function (RestModel) {

        function File(data) {
            this.servicePath = 'post/files';
            if (data) {
                this.setData(data);
            }
        }

        File.prototype = Object.create(RestModel.prototype);

        File.prototype.constructor = File;

        return File;

    }]);