'use strict';
spaApp_model.factory('RestModel', ['Model', '$http',
    function (Model, $http) {

        function RestModel() {
        }

        RestModel.prototype = Object.create(Model.prototype);

        RestModel.prototype.constructor = RestModel;

        RestModel.prototype.get = function (id, params) {
            var self = this;
            self._beforeGet();
            self.cleanParams();
            self.addParams(params);
            return $http.get(serviceBase + self.servicePath + '/' + id, {params: self._params}).then(function (response) {
                self._afterGet(response);
                return self;
            });
        };

        RestModel.prototype.create = function () {
            var self = this;
            self._beforeSave();
            return $http.post(serviceBase + self.servicePath, self).then(function (response) {
                self._afterSave(response);
            }).catch(function (response) {
                self.prepareResponseErrors(response);
            });
        };

        RestModel.prototype.update = function () {
            var self = this;
            self._beforeSave();
            return $http.put(serviceBase + self.servicePath + '/' + self.id, self).then(function (response) {
                self._afterSave(response);
            }).catch(function (response) {
                self.prepareResponseErrors(response);
            });
        };

        RestModel.prototype.delete = function (params) {
            var self = this;
            self.beforeDelete();
            self.addParams(params);
            return $http.delete(serviceBase + self.servicePath + '/' + self.id, {params: self.getParams()}).then(function (response) {
                self._afterDelete(response);
            }).catch(function (response) {
                self.prepareResponseErrors(response);
            });
        };

        return RestModel;

    }]);
